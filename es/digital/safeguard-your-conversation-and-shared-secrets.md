# Protege tu Conversación y Secretos Compartidos

1. ¿Por qué necesitamos una herramienta de comunicación segura?
2. ¿Qué constituye una comunicación segura?
3. ¿Cómo lograr la privacidad y el anonimato?
4. ¿Qué es el intercambio seguro de datos?

(Esta sección introduce a los participantes al concepto básico de cifrado y les presentas herramientas de comunicación y opciones para compartir datos que ofrecen protección.)

**Comunicación Segura: Introducción**

La comunicación segura es esencial cuando dos partes desean conversar sin el riesgo de intercepción o espionaje por terceros. Esto requiere métodos de comunicación que sean inmunes a tales violaciones de privacidad.

1. &#x20;**¿Por qué necesitamos una herramienta de comunicación segura?**

_**Tu dispositivo puede escucharte:**_

* Tus adversarios pueden potencialmente escuchar tus conversaciones explotando el micrófono incorporado en tu teléfono y laptop/PC, particularmente durante llamadas regulares inseguras que son susceptibles de interceptación.

_**Protección de la privacidad e identidad:**_

* Las herramientas de comunicación segura son cruciales para proteger la privacidad y la identidad, especialmente cuando los individuos o sus contactos están en riesgo. Al asegurar la privacidad y la protección de la identidad, se puede reducir el nivel de amenaza, permitiendo a los individuos y redes continuar tu trabajo sin temor a compromisos.

_**Prevención de monitoreo por parte de las autoridades:**_

* En situaciones donde los individuos están bajo constante vigilancia por parte de las autoridades, las herramientas de comunicación segura son esenciales para prevenir el acceso al contenido de las conversaciones, manteniendo así la privacidad y confidencialidad.

_**Mantener a los proveedores de servicios de plataformas fuera:**_

* Los proveedores de servicios de comunicación pueden tener acceso a todas tus conversaciones dependiendo de si la plataforma está cifrada o el nivel de cifrado que proporciona.&#x20;
* Por ejemplo, plataformas como WeChat, donde el proveedor de servicios tiene un vínculo directo con el régimen, han expuesto a defensores en regiones ocupadas como Tíbet y Turkestán Oriental a detenciones, arrestos y sentencias por compartir y recibir información a través de WeChat.

2. &#x20;**¿Qué constituye una comunicación segura?**

_**Seguridad de los datos en tránsito**_

Al seleccionar una plataforma de comunicación, es esencial considerar si ofrecen cifrado y el nivel de protección que brinda a tus datos durante el tránsito. Hay dos tipos de cifrado:

* _¿Qué es HTTPS?_

HTTPS asegura la seguridad de tus datos mientras viajan a su destino. Los mensajes se cifran en tránsito entre el remitente, el proveedor de servicios y el receptor.

* _¿Qué es el cifrado de punto a punto?_

El cifrado de punto a punto asegura que solo el remitente y el receptor puedan ver los mensajes. Esto significa que solo los dispositivos involucrados en el proceso de comunicación pueden cifrar y descifrar los mensajes. Ningún intermediario, incluyendo el proveedor de servicios, tiene la capacidad de descifrar y acceder al contenido de los mensajes. El cifrado de extremo a extremo proporciona el más alto nivel de privacidad y seguridad.

3. &#x20;_**¿Cómo lograr la privacidad y el anonimato?**_

Elegir la herramienta de comunicación adecuada es crítico, especialmente para los usuarios bajo vigilancia. Prioriza plataformas que ofrezcan cifrado de extremo a extremo, asegurando que solo tú y el destinatario puedan acceder al mensaje. Además, las plataformas que ofrecen anonimato proporcionan un nivel ideal de seguridad. También es importante considerar cómo las plataformas manejan tus datos, incluyendo qué información recopilan, cómo se almacena y si se comparte con terceros. También hay que considerar quién está detrás de la plataforma y quién puede tener control sobre ellos. Adicionalmente, para plataformas censuradas y para mejorar las capas de seguridad y privacidad, deberías usar Tor o cualquier VPN confiable o reputada.

| <p><strong>Sugerencia:</strong> (Dependiendo del tiempo disponible para la capacitación)</p><ul><li>Incluye los siguientes gráficos en la presentación de capacitación</li></ul><p>O</p><ul><li>Escribe los gráficos en una pizarra antes de la sesión de capacitación,</li></ul><p>O</p><ul><li>Pide a los participantes que nombren plataformas de comunicación populares que están usando, escríbelas en el gráfico y averigua si la plataforma proporciona cifrado y/o anonimato.</li></ul> |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

<mark style="color:red;">**¡Recuerda!**</mark> <mark style="color:red;"></mark><mark style="color:red;">Si una plataforma no proporciona ningún cifrado, ignórala completamente.</mark>\


**Chat Seguro**

|        <p><br></p>       |                       **Plataforma**                      |                         **Cifrado de Extremo a Extremo**                        |                                                                      **Anonimato**                                                                     |
| :----------------------: | :--------------------------------------------------------: | :----------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------------------------------------------------: |
| <p>Signal</p><p><br></p> |             App (versión de escritorio disponible)             |                                      ✔                                      | <p>Algo - Aunque se requiere un número de teléfono para usar Signal, sin embargo, puedes compartir solo tu nombre de usuario y optar por mantener tu número privado.</p> |
|          Element         | App (versión de escritorio disponible) y basada en navegador (web) |                                      ✔                                      |                             <p>✔</p><p>Sí, si se usa con una cuenta creada sin registro de número de teléfono o correo electrónico.</p>                            |
|            Zom           |               App y basada en navegador (web)               |                                      ✔                                      |                                  <p>✔</p><p>No requiere número de teléfono ni información personal para registrarse.</p>                                 |
|         Whatsapp         | App (versión de escritorio disponible) y basada en navegador (web) |                                      ✔                                      |                                                    <p>✖</p><p>Requiere registro de número de teléfono.</p>                                                   |
|          Convene         |                   Basada en navegador (web)                   | <p>.✔</p><p>Cifrado de punto a punto mientras se usa el Modo Privado</p> |                                                      <p>✔</p><p>No requiere registro.</p>                                                      |



**Llamada de Voz Móvil**\


| <p><br></p> |                       **Plataforma**                      | **Cifrado de Extremo a Extremo** |                                                    **Anonimato**                                                   |                                                                                  **Almacenamiento de Datos**                                                                                 |
| :---------: | :--------------------------------------------------------: | :-------------------------------: | :----------------------------------------------------------------------------------------------------------------: | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
|    Signal   |             App (versión de escritorio disponible)             |               ✔               | <p>✔</p><p>Sí, si se usa con una cuenta creada sin número de teléfono ni información personal para registrarse.</p> |                                                                             No recopila ningún dato.                                                                             |
|   Whatsapp  | App (versión de escritorio disponible) y basada en navegador (web) |               ✔               |                                  <p>✖</p><p>Requiere registro de número de teléfono.</p>                                 | No almacena registros de llamadas en su servidor. Los registros se almacenan localmente en los dispositivos de los usuarios. Pero los metadatos como la duración de la llamada, hora de la llamada, etc. se almacenan temporalmente en su servidor. |
|    Viber    |             App (versión de escritorio disponible)             |               ✔               |                                  <p>✖</p><p>Requiere registro de número de teléfono.</p>                                 | No almacena registros de llamadas en su servidor. Los registros se almacenan localmente en los dispositivos de los usuarios. Pero los metadatos como la duración de la llamada, hora de la llamada, etc. se almacenan temporalmente en su servidor. |
|    WeChat   | App (versión de escritorio disponible) y basada en navegador (web) |               ✖               |                                  <p>✖</p><p>Requiere registro de número de teléfono.</p>                                 |                                                      Retiene datos de llamadas y mensajes, así como información de ubicación del usuario.                                                      |



**Llamada o Conferencia de Video**\


| <p><br></p> |                       **Plataforma**                      | **Cifrado de Extremo a Extremo** |                                                    **Anonimato**                                                   |                                               **Almacenamiento de Datos**                                               |
| ----------- | :--------------------------------------------------------: | :-------------------------------: | :----------------------------------------------------------------------------------------------------------------: | :----------------------------------------------------------------------------------------------------------: |
| Signal      |             App (versión de escritorio disponible)             |               ✔               | <p>✔</p><p>Sí, si se usa con una cuenta creada sin número de teléfono ni información personal para registrarse.</p> |                                           No recopila ningún dato.                                          |
| Element     | App (versión de escritorio disponible) y basada en navegador (web) |               ✔               |           <p>✔</p><p>Sí, si se usa con una cuenta creada sin registro de número de teléfono o correo electrónico.</p>          | No almacena registros de llamadas, pero su proveedor de infraestructura Matrix puede almacenar direcciones IP y marcas de tiempo.  |
| Jitsi       | App (versión de escritorio disponible) y basada en navegador (web) |               ✔               |                <p>✔</p><p>No requiere número de teléfono ni información personal para registrarse.</p>               |                                   No retiene datos de usuario en sus servidores.                                  |
| Google Meet |               App y basada en navegador (web)  |          <p>✖</p><p>Requiere número de teléfono para registrarse        |Google almacena datos pero están cifrados en tránsito y en reposo. |
|WhatsApp | APP (versión de escritorio disponible) y navegador (web) |               ✔   | <p>✖</p><p>Requiere número de teléfono para registrarse  | Según su política de privacidad, Whatsapp no ​​almacena el contenido de los mensajes una vez entregados.   |  