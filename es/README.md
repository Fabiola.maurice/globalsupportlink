---
descripción: ¡Impulsado por la gente y listo para ayudar!
---

# Bienvenido a tu servicio de asistencia

El Servicio de Asistencia proporciona recursos gratuitos a las personas y
organizaciones que forman parte de la red mundial de PxP. Además de la guía de
servicios y los recursos en línea que se describen a continuación, los miembros
del [Help Desk](ask-for-help.md) pueden responder a preguntas y ofrecer
asesoramiento sobre seguridad digital y física a través de los canales de chat,
mensajería o correo electrónico. En función de tus necesidades, el Servicio de
Asistencia también puede organizar:

* Talleres y cursos presenciales sobre seguridad y resistencia para
  organizaciones de la sociedad civil
* Investigaciones sobre seguridad digital
* Apoyo experto en la elaboración de políticas de seguridad
* Reuniones informativas sobre seguridad en los viajes
* Apoyo psicosocial

¡Ponte en contacto hoy mismo con un miembro del [Servicio de
asistencia](ask-for-help.md) para ver qué es lo que más te conviene o a tu
organización!

La guía de servicios abarca tres ámbitos:

* [Seguridad Digital](digital/)
  * Incluye guías de seguridad y privacidad para particulares y organizaciones
* [Seguridad física](physical/)
  * Incluye información sobre la preparación para y durante los viajes y otras
    situaciones
* [Apoyo psicosocial](psychosocial.md)
  * Compartir el acceso a oportunidades y recursos de organizaciones
    comunitarias asociadas

Se trata de un recurso en evolución que se actualizará y mejorará con los
comentarios de todos.

Si necesitas más ayuda de la que se proporciona más arriba, puedes [¡Pedir
ayuda!](ask-for-help.md)
