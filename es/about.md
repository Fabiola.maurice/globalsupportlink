# 👯 Acerca de nosotros

**TibCERT (**[**tibcert.org**](https://tibcert.org)**)** \
El Equipo Tibetano de Preparación para Emergencias Informáticas (TibCERT) se formó para abordar las crecientes amenazas a la ciberseguridad que enfrentan los activistas, periodistas y organizaciones de la sociedad civil tibetanas que operan en entornos políticamente sensibles. Al reconocer la necesidad crítica de mejorar la conciencia y la resiliencia de la seguridad digital, TibCERT tiene como objetivo capacitar a personas y organizaciones de todo el mundo con el conocimiento, las herramientas y los recursos necesarios para defenderse contra las amenazas cibernéticas de manera efectiva.

**Keri cuidando a los activistas (**[**facebook.com/KeriMentalHealth/**](https://facebook.com/KeriMentalHealth/)**)**\
Un colectivo de activistas que apoyan la salud mental y el bienestar de los defensores de la justicia social. Brindan justicia social y apoyo psicosocial y de salud mental basado en la psicología indígena/crítica a activistas en riesgo, al mismo tiempo que practican atención basada en el trauma y asesoramiento afirmativo LGBTQ+. También brindan asesorías de seguridad psicosocial y talleres de atención colectiva a movimientos y organizaciones de derechos.

**Videre (**[**videreonline.org**](https://videreonline.org)**)**\
La metodología de Videre supera un problema fundamental de la documentación tradicional sobre derechos humanos: su incapacidad para centrar y elevar las voces y el conocimiento de primera línea. Muchas comunidades enfrentan importantes desafíos de seguridad al recopilar los detalles de las violaciones que sufren, y los problemas de verificación y difusión obstaculizan su eficacia y utilidad. Superamos esto desarrollando relaciones estrechas y duraderas con activistas y comunidades locales, llegando a áreas donde viven los más vulnerables y aprovechando el poder del conocimiento local.

**Guardian Project (**[**guardianproject.info**](https://guardianproject.info)**)**

Con 15 años de experiencia en el espacio de la libertad en Internet, Guardian Project trabaja para dar voz a todas las personas, mejorar la seguridad y brindar acceso al conocimiento, independientemente de su ubicación o conectividad. Construimos nuestro trabajo juntos sobre software gratuito y de código abierto, cifrado de extremo a extremo, procesos de diseño ético, auditorías de código y accesibilidad, localización colaborativa y otros enfoques mutuamente beneficiosos para ofrecer soluciones basadas en tecnología.