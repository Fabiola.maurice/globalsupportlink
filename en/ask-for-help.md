---
description: All the ways you can reach out to ask for help
---

# ☎️ Ask for Help!

**This service is still being fully rolled out.** Currently, we can provide **advice and technical support** on topics related to **digital & physical security** and **psychosocial resilience**. In some cases we may connect you with a regional partner or organisation that is better placed to assist.

**Note:** We are not in a position to provide extensive legal support, and we do not currently conduct advocacy activities.&#x20;

* Public Help Desk Chat: [chat.globalsupport.link](https://chat.globalsupport.link)
*   Help Desk Support:

    * Email: [help@globalsupport.link](mailto:help@globalsupport.link)
    * WhatsApp: [+447886176827](https://wa.me/+447886176827)
    * Telegram: [@GlobalSupportLink\_bot](https://t.me/GlobalSupportLink\_bot)



_This help desk is powered by_ [_free, open-source, secure and audited software_](https://digiresilience.org/solutions/link/)_, and hosted in a privacy-focused, environmentally concerned, and physically controlled_ [_datacenter_](https://greenhost.net/)_._
