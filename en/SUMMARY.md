# Table of contents

* [Welcome to Your Help Desk](README.md)
* [📱 Digital Security](digital/README.md)
  * [Keeping Your Devices and Physical Space Safe](digital/keeping-your-devices-and-physical-space-safe.md)
  * [Safeguard your Conversation and Shared Secrets](digital/safeguard-your-conversation-and-shared-secrets.md)
  * [You are the Best Antivirus](digital/you-are-the-best-antivirus.md)
  * [Better Living through Threat Modeling!](digital/better-living-through-threat-modeling.md)
* [🛅 Physical Security](physical/README.md)
  * [Analyzing Risk](physical/analyzing-risk.md)
  * [A Safe Trip: It's All in the Preparation](physical/a-safe-trip-its-all-in-the-preparation.md)
  * [Device Security: Preparing for Theft, Loss or Confiscation](physical/device-security-preparing-for-theft-loss-or-confiscation.md)
  * [Hotel Security](physical/hotel-security.md)
  * [Tips to Mitigate Common Risks](physical/tips-to-mitigate-common-risks.md)
* [❤️ Psychosocial Support](psychosocial.md)
* [👯 About Us](about.md)
* [☎️ Ask for Help!](ask-for-help.md)
