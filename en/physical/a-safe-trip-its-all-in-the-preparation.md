---
description: What to document and establish prior to traveling
---

# A Safe Trip: It's All in the Preparation

### Key Contacts & Locations

It is important to gather as much information as possible regarding the places and people you'll be visiting prior to travel. Below is a template for listing the contacts and locations. It is important to share the following information with family or colleagues before you travel. Try to be as specific as possible and include screenshots of relevant maps, such as from the airport to the hotel, as much as you can. This is one of the ways that others can try to locate you if you are in trouble or not checking in. It also may be useful to share your agenda with contacts back home.

| Destination Country                                                    | <p><br></p> |
| ---------------------------------------------------------------------- | ----------- |
| Destination Cities                                                     | <p><br></p> |
| Dates                                                                  | <p><br></p> |
| Fellow Travellers                                                      | <p><br></p> |
| Security/head office backstop                                          | <p><br></p> |
| Flight information (airline, flight number, departure & arrival times) | <p><br></p> |
|                                                                        |             |

| Local Contact/Partner Information |             |
| --------------------------------- | ----------- |
| Name & organization               | <p><br></p> |
| Phone                             | <p><br></p> |
| Email                             | <p><br></p> |

| Accommodation Information |             |
| ------------------------- | ----------- |
| Hotel/residence           | <p><br></p> |
| Phone                     | <p><br></p> |
| Email                     | <p><br></p> |
| Address/GPS               | <p><br></p> |

| Office / Meeting Location Information |             |
| ------------------------------------- | ----------- |
| Venue                                 | <p><br></p> |
| Phone                                 | <p><br></p> |
| Email                                 | <p><br></p> |
| Address/GPS                           | <p><br></p> |

### Pre-Travel Administrative & Logistic Checklist

In addition to listing out the contacts and locations, it can be useful to keep a checklist to ensure that you’ve covered all of the logistical and administrative aspects of the trip. It is especially important to establish a regular check-in procedure with someone back home (either a family member or colleague). This can be as simple as establishing that you’ll send a Signal message that you’re okay every day at 17:00. Below is a template that can help you ensure you have everything prepared prior to your trip.

| Provide specifics or additional information as necessary |                                                                                                                                                                                                                                                        |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| ☐                                                        | <p>What is your "official story?" You may not be able to be upfront about what you are doing in country. Make sure that you have a solid story (that you’ll be able to remember easily) and that you’ve shared it with your colleagues.</p><p><br></p> |
| ☐                                                        | <p>Visa</p><p><br></p>                                                                                                                                                                                                                                 |
| ☐                                                        | <p>Pre-departure security briefing with a local partner or contact</p><p><br></p>                                                                                                                                                                      |
| ☐                                                        | <p>Communications / check-in procedure established with head office or family member </p><p><br></p>                                                                                                                                                   |
| ☐                                                        | <p>Signal group created with travelers and security/head office backstop</p><p><br></p>                                                                                                                                                                |
| ☐                                                        | <p>Emergency cash available or bank card approved for international use</p><p><br></p>                                                                                                                                                                 |
| ☐                                                        | <p>Airport pick-up arranged</p><p><br></p>                                                                                                                                                                                                             |
| ☐                                                        | <p>Nearest medical facilities identified</p><p><br></p>                                                                                                                                                                                                |
| ☐                                                        | <p>Insurance coverage (health, medical evacuation, etc.)</p><p><br></p>                                                                                                                                                                                |
| ☐                                                        | <p>Decision on visibility/profile - will you be low-profile or can you be open about what you and/or your organisation are doing in country?</p><p><br></p>                                                                                            |

### When travelling, don't forget to...

* Register with your embassy or consulate before travel. It is possible to due this online for several nationalities.
* Keep your home office backstop informed of daily plans utilizing your Signal group.
* Pre-program emergency numbers into your phone, and keep a separate emergency contact list in case your phone is stolen or lost.
* Learn key words or phrases in the local language to signal for help.
* Maintain a low profile; dress and behave appropriately and be considerate of local customs.
* Avoid political discussions, and respect cultural sensitivities.
* Get enough sleep, manage stress, and avoid abusing drugs or alcohol in order to respond appropriately during a potential or actual safety or security incident.
* Choose airlines with [IATA certification](https://www.iata.org/en/about/members/airline-list/).
* Only take licensed taxis or Ubers and always settle on the fare BEFORE beginning the trip. Have the destination address written in the local language to show the driver if necessary. When in the vehicle, ensure that all doors and windows can be securely closed and locked.
* Stay calm in all situations; be non-provocative when confronted with actual or potential hostile situations.
* Learn to recognize signs of threats and be conscious of escape routes.
* Maintain constant situational awareness of surroundings and broader operational context. Stay alert, and listen to people’s advice.

