---
description: Best Practices for Safety & Security
---

# 🛅 Physical Security

As Human Rights Defenders, we can sometimes find ourselves at our most vulnerable: traveling in unfamiliar terrain and potentially meeting new people or doing things we wouldn't otherwise do.&#x20;

In this section, you'll discover some ways to anticipate security risks as well as measures you can put in place to prevent or respond to them.&#x20;

Remember, every context has its specific dangers and every person has their specific vulnerabilities - only by identifying both of these aspects, can we determine the security risks.

