---
description: What to do in the event of...
---

# Tips to Mitigate Common Risks

### Surveillance & Counter-surveillance

Your movements or activities could be physically monitored by a hostile individual or group in order to determine your vulnerabilities and/or plan an attack. Some signs that you’re under physical surveillance could include:

* Strangers asking intrusive questions about your colleagues or work
* Seeing the same stranger in several different places
* A suspicious number of calls from people claiming to have reached a wrong number
* Being followed, either on foot or in a vehicle
* New or unusual vendors, beggars, loiterers, etc., around your home or office

Observing the following precautions may increase safety:

* Avoid predictable routines such as jogging or shopping at the same time every day, especially when alone
* Travel with others and in groups, and with trusted local partners to the extent possible
* Change routes and travel times regularly
* Use different vehicles if possible, and alter routes, times, and sequences – sharing information on a “need to know” basis
* Avoid attracting attention to yourself: do not wear expensive watches or jewellery or show large quantities of cash; dress modestly; do not talk on the phone or wear headphones while walking in public
* If you feel that you are under surveillance, proceed to a secure, well-lit location where others are present and seek assistance.

### Sexual Harassment

Sexual harassment is not limited to women – men and women, of all ages, ethnicity and economic groups are at risk. However, certain aspects of your individual profile, such as gender identity, sexual orientation or ethnicity can make you more vulnerable to sexual harassment. Sexual harassment and assault are under-reported; victims are often targeted in advance and the assailant is often a known acquaintance. It is **never** your fault if you are sexually harassed. Observing the following precautions may increase safety, but are not foolproof and, ultimate responsible always lies with the perpetrator:

* Listen to instincts - if something does not feel right, leave the situation as quickly as possible.
* Personal boundaries (privacy, personal space, etc.) are different everywhere. Ask about, and follow, local cues.
* Dress appropriately, with respect for the local culture.
* When staying at a hotel, try to receive any guests in the lobby or in a neutral area, not your hotel room.
* Keep personal information private. Do not disclose personal information such as hotel name, address, or cell phone number. Offer to take the person’s contact information rather than providing your own.
* Always report any suspicious or threatening behavior to your organisation or someone you feel safe with.

### Civil Unrest

Civil unrest can occur for a variety of reasons, most of which are related to social or political issues. Countries undergoing widespread political upheaval may experience sustained bouts of politically motivated unrest ranging from small, unorganized rallies to large-scale demonstrations and rioting, any of which can turn violent with little to no warning. While it is rare for visitors to be directly targeted during incidents of unrest, observing the following precautions may increase safety:

* Monitor local and international news.
* Familiarize yourself with local conditions. Ascertain whether there have been recent incidents of unrest, and if so, their cause, severity and how the authorities responded.
* Avoid areas where demonstrations are taking place or stay indoors until it becomes clear that the situation has stabilized.
* Leave the area and find an alternate route to the intended destination if you find yourself near an area of unrest.
* Do not travel alone to areas where there is a strong likelihood of demonstrations.

If you find yourself in crowd:

* Keep close to, and maintain visual contact with the person you are with.
* Avoid any situation where police or security forces are actively engaged with demonstrators. Authorities in some countries do not tolerate dissent and may use excessive force in an effort to quell unrest.
* Keep calm, as crowds are likely to dissipate in a short period of time.
* Keep to the edge of the crowd, and stay away from leaders and agitators. Try to avoid being identified as one of the demonstrators.
* Create space in a crowd by grasping wrists and bracing elbows away. Bending over slightly will allow breathing room.
* Stay clear of glass shop fronts, and move with the flow of the crowd.
* If pushed to the ground, try to get against a wall, roll into a tight ball and protect head with hands until the crowd dissipates.
* Break away and seek refuge in a nearby building at the first opportunity. Alternatively, find a suitable doorway or alley and remain there until the crowd passes.
* Avoid drawing attention. When leaving the area of a demonstration, walk away slowly and avoid the temptation to run.
* If shooting breaks out, try to find cover.
* If arrested by security forces, do not resist.

### Public Transportation

The high density of people using public transportation creates ideal conditions for criminal conduct as it increases the number of potential victims and provides anonymity to the perpetrator. Common incidents may include petty crime such as theft or pick- pocketing, or more serious incidents such as kidnapping. Observing the following precautions may increase safety:

* Never hitchhike or accept a ride from strangers
* Avoid traveling alone
* Have the proper token or change ready when approaching the ticket booth or machine
* Be mindful of pickpockets and thieves while waiting for transportation
* Wait for the bus or train in well-lit, designated waiting areas, particularly during off-peak hours
* Leave any public transportation that feels uncomfortable or threatening.
* After getting off, check to be sure no one is following.
* Avoid traveling by bus or train at night. If this is unavoidable:
  * Avoid empty buses or train cars
  * Sit near the driver on buses or in the middle car with other passengers on trains
  * Sit by a window or near doors, which allow for a quick exit in the event of an accident

### Walking

* Use well-travelled and well-lit routes and seek advice on local areas considered safe for walking.
* Check a local street map or Google Maps before leaving.
* Avoid walking alone or at night.
* Walk with confidence, but stay alert to potential problems.
* Do not wear headphones, which can reduce situational awareness and might indicate wealth.
* Avoid walking too close to bushes, dark doorways, and other places where criminals might hide.
* Be aware of jostling in crowded areas. Divide money and credit cards between two or three pockets or bags, as pickpockets often work in pairs and use distraction as their basic ploy.
* Keep backpack or purse close to the body to prevent snatch-and-run theft. Don’t carry valuables in these bags; instead, leave valuables in a secure place.
* Carry only a small amount of money and a cheap watch to hand over if threatened.
* If a driver pulls alongside to ask for directions, don’t approach the vehicle.
* If someone seems suspicious, cross the street or change directions to get away from the person. If necessary, cross the street several times. If the person is following or becomes a threat, use whatever means necessary to attract another bystander’s attention. Yelling “Fire!” often attracts more attention than yelling “Help!” Remember, it is better to be embarrassed for being overcautious than be a victim of crime.

### Theft or Intimidation

* Do not try to intimidate the assailant or be aggressive. Instead, be polite, open, and confident; try not to show anger or fear.
* Speak calmly and clearly.
* Keep hands visible, and move slowly with precise gestures.
* Respond to requests, but don’t offer more than what is requested.
* If in a group, do not talk more than is necessary, particularly in a language the assailants do not understand.
* Report the incident to your organisation or the authorities as soon as possible.
* Life cannot be replaced, never risk life to protect property or money.
