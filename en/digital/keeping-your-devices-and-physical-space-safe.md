# Keeping Your Devices and Physical Space Safe

1. Don’t Let Anyone Into Your Device
2. Your Device Can Track or ID You
3. Don’t Let Bad Apps Crash Your Party
4. Enable ‘Find My Phone’

This section will primarily address mobile devices, given their status as primary tools for communication, particularly for defenders on the move. However, the practices covered in this training are equally applicable to laptops/PCs.

1. **Don't Let Anyone Into Your Device**

Don’t give anyone easy access to your device. Doing so would provide them unfettered access to all personal and work-related information stored on your device, including contacts, emails, documents, photo gallery, communication history, and more.

| <p><strong>Suggestion:</strong> </p><ul><li><p>Here, you can encourage participants to share various methods for locking their devices.</p><ul><li>Answers to expect: FaceID, Pin, Biometrics, Pattern, Passphrase </li></ul></li></ul><ul><li><p>Following that, you can ask participants which device lock option they consider ideal for them and why, or you can go through each option and ask participants if they believe the option is secure.</p><ul><li>FaceID: This option lacks security as anyone could potentially coerce you into unlocking the device.</li><li>PIN: A 4-digit PIN is susceptible to easy cracking. Anyone with sufficient time to attempt 10,000 PIN combinations or access to a PIN cracking device can compromise it.</li><li>Biometrics: Similar to FaceID, biometrics can also be coerced, rendering it insecure for defenders.</li><li>Pattern: Patterns are often simple and easy to remember, making them susceptible to cracking.</li><li>Passphrase: Despite being less convenient, a passphrase stands out as the most secure option for locking devices, especially for defenders at risk of device theft or confiscation. <mark style="color:red;"><strong>* Although this may not be the optimal choice for use in crisis situations. In such cases, having an alternative device is advisable. We will delve deeper into this topic shortly.</strong></mark></li></ul></li></ul> |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |

2. **Your Device Can Track or ID You**

_**Your Device Can ID You**_

When you purchase a SIM card, your SIM provider will collect your personal information for registration. Each time you make a call, the cell tower retains a record of your International Mobile Equipment Identity (IMEI) and International Mobile Subscriber Identity (IMSI), which can be utilized for identification purposes in the future.

* _What is IMSI?_

IMSI stands for International Mobile Subscriber Identity, a unique identification number linked to a cell phone user. It is stored in the SIM card and contains the user's specific details.

\* Users will have to contact their service provider to know that IMSI number. IMSI number may also be found on the SIM card package users get at time of subscription.

* _What is IMEI?_

IMEI stands for International Mobile Equipment Identity, a unique identification number assigned to mobile phones and certain satellite phones.

| <p><strong>Suggestion:</strong></p><ul><li>If  participants are allowed to keep their mobile device with them during the training, you can ask them to dial *#06# on their phone to know what their device IMEI number is.</li></ul><p>OR</p><ul><li>Android Devices: Device ‘Settings’ > ‘About Phone’ </li><li>iOS Devices: ‘Settings’ > ‘General’ > ‘About’</li></ul> |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |

_How are IMSI and IMEI related?_

Once you register your SIM and start using it, both IMSI (International Mobile Subscriber Identity) and IMEI (International Mobile Equipment Identity) numbers become interconnected and registered together with a cellular tower. Therefore, if you ever feel compromised, simply changing your SIM card or phone won't suffice. It's advisable to change both your SIM card and your phone for added security.

_**Your Device Can Track You**_

Mobile Service Providers can locate you through Cell Tower Triangulation. The combination of your identity recorded by the cell tower using your IMEI and IMSI, and Cell Tower Triangulation, service providers can tell where you are.

_What is Cell Tower Triangulation?_

Cell Tower Triangulation is a widely used technique to determine the location of a phone or device. When a cell phone signal is detected by three or more cell towers, triangulation can be employed. By pinpointing the overlap of signals from these towers, it becomes possible to estimate the location of a cell phone based on its distance from each of the three towers.

<figure><img src="https://lh7-us.googleusercontent.com/vWGy4akgpuKhIhdKjBZ7EEtFF3FJchBhrIF_DUL6K5PHEncB3_SG0kqZr2xDIRsOWZPryy8Uu7v9KXu04wMdOylQw9b2ltEPRNpAuPYNNGgfNGzuzJREg8PDSqdAq715WOmm8srRrDj9LFIeN7eA_zgDpg=s2048" alt="" width="375"><figcaption><p>Cell Tower Triangulation</p></figcaption></figure>

| <p><strong>Suggestions:</strong> (Depending on time available for the training)</p><ul><li>Trainer can either explain what ‘Cell Tower Triangulation’ is</li></ul><p>OR</p><ul><li>The trainer can ask whether participants are familiar with how a mobile phone can assist in tracking them, or if they know the concept of cell tower triangulation.</li></ul><p>OR</p><ul><li>You can either draw a diagram illustrating how triangulation works</li></ul><p>OR</p><ul><li>Invite four participants to volunteer. Position three participants as cell towers in a formation that creates a triangle, and designate one participant as the person with the phone to stand in between the towers. Then, explain how the three towers continuously establish connections with the person's phone. Clarify that the phone's location is determined based on the time it takes for signals to return to each tower. As the person moves around, the connection between the towers persists. Once the person moves further away from the towers, the device connects to other towers, allowing for continuous tracking of the phone's location.</li></ul> |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |

| <p><strong>Absolutely Gotta!</strong></p><ul><li>If you suspect you're being tracked, consider changing both your phone and SIM card, preferably opting for unregistered ones or those not under your name, as this may help evade tracking, unless you're being physically followed.</li><li><p>When participating in a protest where involvement may pose risks, it's advisable not to bring your phone with you. Here is what you can do:</p><ul><li>If you need to communicate with your network during the protest, consider using a new phone, and ideally, refrain from using a SIM card altogether; instead, utilize WiFi networks and keep switching networks to connect. However, if you must use a SIM card, try to obtain an unregistered SIM card if possible, along with a new mobile device. Remember! Using your regular phone with a new SIM card won’t help since your device's IMEI has already been associated with your regular SIM card.</li><li>If communication with your network isn't necessary during the protest but you require a device to document it, opt for a small camera or a new mobile device. Your regular phone might continue connecting to the cell tower and register your IMEI even without a SIM card.</li></ul></li></ul> |
| ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |

3. &#x20;**Don’t Let Bad Apps Crash Your Party**

{% embed url="https://www.youtube.com/watch?index=2&list=PL0Hcq8UCiYqkUVnpduzynZw9dpGRcqNhs&v=TbRrFWy5_t0" %}

The first topic we discussed, "Don’t Let Anyone Into Your Device," focused on preventing external entities from accessing your device. Now, we'll delve into how to prevent entities already within your device from accessing various data and features.

| <p><strong>Suggestion:</strong> </p><p>Here, we can ask whether participants review app permissions when installing a new app or checking permissions of apps already in use. Depending on their response, you can then prompt them to elaborate on their reasons for doing so or not doing so.</p> |
| --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
|                                                                                                                                                                                                                                                                                                     |

Most people don’t have the habit of reviewing app permissions when installing or using an app. Often, apps only request permissions to access features and data that could enhance the functionality. However, there are many apps  that can be sneaky. Here are few considerations before installing an app:

* _Ask yourself, 'do I really need this app?'_
  * Having numerous apps on your device consumes storage space and can slow down your device.
  * It also implies that more apps have access to your data on the device.
* _Choose your apps wisely_
  * Mobile phones carry our lives in it. We have our family and friends, our work contacts, all our communications, memories, banking, finance, everything on them. We must choose apps with a good track record and a good user privacy policy.
  * Know who owns the app.
  * Opting for an app with a server in your country may pose potential risks, particularly if there are laws mandating app service providers to share data with the government.
  * Similarly, selecting an app with its owner closely associated with the regime you are resisting may also pose a potential risk.
  * Moreover, if the app service provider has a track record of handing over information of users, especially from civil society, to the government, there may be potential risks as well.
* _Is the app requesting excessive information?_
  * App permissions dictate what your app can do and access. Not all apps are secure, so it's crucial to review their permissions rather than granting blanket access to device features and data like the camera, microphone, location, calendar, email, contacts, etc. The real risk of app permissions lies in their potential misuse.
  * When installing an app, always scrutinize the permissions it requests. If an app seeks permissions such as device administration, access to Wi-Fi information, or personal data it doesn't need to function, refrain from installing it.
    * For example, a flashlight app's primary function is to provide light, so it makes sense for it to require access to the camera since it utilizes the camera flash for illumination. However, if the flashlight app demands access to your contacts, call logs, or photo gallery, it raises suspicion. Many of these apps behave similarly to malicious software, prioritizing access to our data.

4. &#x20;**Enable ‘Find My Phone’**

| **Reminder:** This feature only works when you phone is connected to WiFi or data |
| --------------------------------------------------------------------------------- |

This feature is typically utilized for locating your device if it's lost or misplaced. However, certain features in Find My Phone can be valuable for defenders, especially when your device is confiscated or falls into the hands of adversaries.&#x20;

Here's how you can enable Find My Phone:

* iPhone: [https://support.apple.com/en-ca/102648](https://support.apple.com/en-ca/102648)
* Android: [https://support.google.com/accounts/answer/3265955?hl=en](https://support.google.com/accounts/answer/3265955?hl=en)

When your device is confiscated or in the possession of adversaries, consider the following actions based on your situation:

* Log in to android.com/find or icloud.com/find depending on what device you are using.
  * Remotely set up a device PIN number if you haven't already done so. This can delay others' access to your device.
  * Remotely erase data on your device if you have any information that could compromise your safety and the safety of your network.
