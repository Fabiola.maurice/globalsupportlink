---
description: Focused on best practices for communication and data privacy and security
---

# 📱 Digital Security

## **Leveling Up Your Digital Armor!** &#x20;

Shielding Your Digital Fortresses

**Purpose:**

This digital security training is to empower human rights activists, journalists, lawyers, and other participants with the knowledge and skills necessary to protect themselves and their organizations against cyber and physical threats. By equipping participants with practical cybersecurity strategies and tools, the training aims to enhance their digital resilience and mitigate the risks of data breaches, surveillance and other forms of cyber attacks.&#x20;

**Outcome:**

* Increased awareness: Participants develop heightened awareness of cybersecurity threats and vulnerabilities.
* Enhanced security practices: Participants acquire practical skills for securing devices, communications, and sensitive information.
* Readiness for incidents: Participants will be equipped to apply acquired skills and knowledge effectively in real-life incidents scenarios.&#x20;

**Process:**

During this digital security training, participants will engage in various modules aimed at equipping them with the knowledge and skills to defend against cyberattacks. The training will commence with strategies for securing mobile devices, followed by essential best practices for maintaining cybersecurity. Additionally, they will explore methods for communicating securely with privacy and anonymity. The training will also include a threat modeling exercise, where participants will apply their knowledge and skills to different scenarios.&#x20;

**Introduction:**&#x20;

Digital security is the shield individuals and organizations use to safeguard information. In numerous situations, a failure to defend may result in physical threat.

While digital security may appear highly technical and daunting to many, it essentially relies on simple steps that are easy to understand and implement. This training serves as an introduction (or reminder) to these fundamental practices.

This training will cover the following module. But each module can be trained separately as well depending on the needs of the target community.\
